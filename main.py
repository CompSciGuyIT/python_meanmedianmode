# A simple program to find the mean, median, and mode of a set of given values

def mean(some_list):
    total = 0
    for i in some_list:
        total += i
    return total / len(some_list)


def median(some_list):
    some_list.sort()
    return some_list[len(some_list)//2]


def mode(some_list):
    my_num = -1
    count = 0
    my_dict = {}

    # Add list members to dictionary
    for i in some_list:
        if i in my_dict:
            my_dict[i] = my_dict.get(i) + 1
        else:
            my_dict[i] = 1

    # Find member with highest count
    while my_dict:
        item = my_dict.popitem()
        if item[1] > count:
            my_num = item[0]
            count = item[1]

    return my_num

### Main ###
my_list = []

# Get user input
while True:
    num = input("Enter number, or just hit <Enter>:")
    if num == "":
        break
    else:
        my_list.append(float(num))

# Display results
print(f"""\n{my_list}
Mean: {mean(my_list)}
Median: {median(my_list)}
Mode: {mode(my_list)}""")

